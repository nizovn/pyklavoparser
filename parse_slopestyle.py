# -*- coding: utf-8 -*-

import sys
import numpy as np
import re

files_int = list(range(1,21))
#files_int.remove(1)
files = [str(f)+".html" for f in files_int]
#print(files)
num_slopestyle_races = 8
min_player_finishes = 5
min_mileage = 1000
short_title = False
min_num_results_for_award = 4

def is_player_string(s):
    return (s.find("onmouseout=\"hideProfilePopup") > -1)

def split_player_strings(s):
    return s.split("div class=\"player")

def parse_name(s):
    match = re.search("onmouseout=\"hideProfilePopup\([0-9]*\);\">(.*?)</a>", s)
    if match:
        return match.group(1)
    return None

def parse_id(s):
    match = re.search("onmouseout=\"hideProfilePopup\(([0-9]*)\);\">", s)
    if match:
        return match.group(1)
    return None

def parse_error_percent(s):
    match = re.search("ошиб.. \(<span class=\"bitmore\"><span class=\"bitmore\">(.*?)</span></span>%\)", s)
    if match:
        return float(match.group(1).replace(',','.'))
    return None

def parse_error_count(s):
    match = re.search("<span class=\"bitmore\">([0-9]*)</span></span> ошиб", s)
    if match:
        return match.group(1)
    return None

def parse_speed(s):
    match = re.search("<span class=\"bitmore\"><span class=\"bitmore\">([0-9]*)</span></span> <span id=\"(.*)\">зн/мин</span>", s)
    if match:
        return int(match.group(1))
    return None

def parse_place(s):
    match = re.search(">([0-9]*) место</ins>", s)
    if match:
        return match.group(1)
    return None

def parse_rang(s):
    match = re.search("class=\"rang([0-9]*) profile", s)
    if match:
        return match.group(1)
    return None

def parse_award_mileage(s):
    match = re.search("за ([0-9]*) текстов пробега\">", s)
    if match:
        return int(match.group(1))
    return None

class Table(dict):
    def add_missing_games(self, _id, n):
        for k in self[_id].keys():
            self[_id][k].extend([None]*n)

    def add_this_game(self, data):
        _id = data["id"]
        for k in self[_id].keys():
            self[_id][k].extend([data[k]])

    def add_new_id(self, data):
        _id = data["id"]
        self[_id] = {}
        for k in data.keys():
            self[_id][k] = []

    def add_data(self, data):
        _id = data["id"]
        if _id in self.keys():
            missed_games_from_last_visit = data["game_number"] - len(self[_id]["id"]) - 1
            if missed_games_from_last_visit>0:
                self.add_missing_games(_id, missed_games_from_last_visit)
            self.add_this_game(data)
        else:
            self.add_new_id(data)
            self.add_missing_games(_id, data["game_number"]-1)
            self.add_this_game(data)

    def add_missing_last_games(self):
        for _id, value in self.items():
            missed_last_games = num_games - len(value["id"])
            if missed_last_games>0:
                self.add_missing_games(_id, missed_last_games)

# парсинг

table = Table()

num_games = 0

for i,file in enumerate(files):

    try:
        bu = open(file, "r", encoding="utf-8")
    except:
        continue

    num_games += 1
    players_in_this_race = []
    bad_players = 0 # учет задвоений

    for line in bu:
        if not is_player_string(line):
            continue
        player_strings = split_player_strings(line)
        for s in player_strings:
            if not is_player_string(s):
                continue
            data = {}
            data["name"] = parse_name(s)
            data["id"] = parse_id(s)
            data["error_percent"] = parse_error_percent(s)
            data["error_count"] = parse_error_count(s)
            data["speed"] = parse_speed(s)
            data["place"] = parse_place(s)
            data["rang"] = parse_rang(s)
            data["award_mileage"] = parse_award_mileage(s)
            data["game_number"] = num_games

            if data["name"] not in players_in_this_race:
                table.add_data(data)
                players_in_this_race.append(data["name"])
            else:
                print("WARNING: несколько игроков с одним ником в заезде {} {}".format(file, data["name"]))
                bad_players += 1
                data["id"] = -bad_players
                data["name"] = data["name"]+"-BUG"+str(bad_players)
                table.add_data(data)
                min_player_finishes = 0
                min_mileage = 0

table.add_missing_last_games()

def list_get_last_not_none(_list):
    list_filtered = list(filter(None, _list))
    if len(list_filtered)>0:
        return list_filtered[-1]
    else:
        return None

def list_get_first_not_none(_list):
    list_filtered = list(filter(None, _list))
    if len(list_filtered)>0:
        return list_filtered[0]
    else:
        return None

# подсчет результатов
results_dict = {}
for _id, value in table.items():
    list_all = []
    for i, s in enumerate(value["speed"]):
        item = {}
        item["speed"] = s if s is not None else 0
        item["game"] = i
        list_all.append(item)

    all_sublists = [ list_all[i:i+num_slopestyle_races] for i in range(len(list_all)-num_slopestyle_races+1)]

    for i, _list in enumerate(all_sublists):
        _list = sorted(_list, key = lambda x: x["speed"])
        all_sublists[i] = _list

    all_sublists = sorted(all_sublists, key = lambda x: [_x["speed"] for _x in x], reverse = True)
    results_dict[_id] = all_sublists[0]

for _id, value in table.items():
    speed = [ _s if _s is not None else np.nan for _s in value["speed"] ]
    avg_speed = np.nanmean(speed)
    table[_id]["speed_normalized"] = [_s/avg_speed if _s and avg_speed else None for _s in value["speed"]]
    table[_id]["avg_speed"] = avg_speed
    error_percent = [ _e if _e is not None else np.nan for _e in value["error_percent"] ]
    avg_error_percent = np.nanmean(error_percent)
    table[_id]["avg_error_percent"] = avg_error_percent
    table[_id]["num_finishes"] = sum(~np.isnan(speed))
    table[_id]["classified"] = (list_get_last_not_none(table[_id]["award_mileage"]) or 0) >= min_mileage \
                               and (results_dict[_id][0]["speed"] > 0)

def sort_func(x):
     _speed = [ _x["speed"] for _x in x[1] ]
     _speed.insert(0, table[x[0]]["classified"])
     return _speed

results_sorted = sorted(results_dict.items(), key = sort_func, reverse = True)

results_sorted = [ _x for _x in results_sorted if table[_x[0]]["num_finishes"]>=min_player_finishes ]

# подсчет сложности текстов
difficulty_dict = {}
for game in range(num_games):
    normalized_speed = [ table[_id]["speed_normalized"][game] or np.nan for _id in table.keys() ]
    avg_difficulty = np.nanmean(normalized_speed)
    difficulty_dict[game] = avg_difficulty

# таблица
out_table = []
row = ["№","Ник","Резуль-\nтат"]
for i in range(1,num_games+1):
    row.append(i)
row.extend(["Сред.\nскор.","Сред.\nошиб."])
if not short_title:
    row = [ _c.replace("-\n","") if isinstance(_c, str) else _c for _c in row ]
    row = [ _c.replace( "\n","") if isinstance(_c, str) else _c for _c in row ]
out_table.append(row)
out_table.append(["" for _i in range(len(row))])

def rounded_float(_s, _accuracy):
    s = round(_s, _accuracy)
    if np.isnan(s):
        return ""
    return s

for position, (_id, best_list) in enumerate(results_sorted):
    value = table[_id]
    row = []
    row.append(position+1 if value["classified"] else "")
    row.append(list_get_last_not_none(value["name"]))
    row.append(best_list[0]["speed"])

    for i in value["speed"]:
        row.append(i or "")

    row.append(rounded_float(value["avg_speed"], 2))
    row.append(rounded_float(value["avg_error_percent"], 3))
    out_table.append(row)

'''
# вывод в data URL
html_table = "<tt><table border=\"1\">"
for row in out_table:
    html_table += "<tr>"
    for cell in row:
        html_table += "<td>"+str(cell).replace("\n","<br>")+"</td>"
    html_table += "</tr>"
html_table += "</tt>"

import base64
urlSafeEncodedBytes = base64.b64encode(html_table.encode("utf-8"))
urlSafeEncodedStr = str(urlSafeEncodedBytes, "utf-8")
data_url = "delete.com?data:text/html;base64," + urlSafeEncodedStr

f = open("out.txt","w")
f.write(data_url)
f.close()
'''

# вывод в эксель
import pandas as pd
df = pd.DataFrame(out_table)
#print(df)

xlsFilepath = "out.xlsx"

writer = pd.ExcelWriter(xlsFilepath, engine='xlsxwriter')
workbook = writer.book
sheet_name = "Таблица"
worksheet = workbook.add_worksheet(sheet_name)
x_offset, y_offset = (1,1)

cell_height = 18 # 15 default
worksheet.set_default_row(cell_height)

for i, col in enumerate(df.columns):
    # максимальная ширина строк в столбце с учетом переносов
    column_len = df[col].astype(str).str.split(pat="\n").map(lambda x: max(x, key=len)).astype(str).str.len().max()
    column_len = max(column_len, len(str(col))) + 1
    if i == 1: # столбец с никами, жирный
        column_len *= 1.2
    worksheet.set_column(i+x_offset, i+x_offset, column_len)

def color_for_rang_fg(r):
    colors = {}
    colors["1"] = "#8d8d8d"
    colors["2"] = "#4f9a97"
    colors["3"] = "#187818"
    colors["4"] = "#8c8100"
    colors["5"] = "#ba5800"
    colors["6"] = "#bc0143"
    colors["7"] = "#5e0b9e"
    #colors["8"] = "#2e32ce"
    colors["8"] = "#00037c"
    colors["9"] = "#061956"
    return colors[r].upper()

def color_for_rang_bg(r):
    colors = {}
    colors["1"] = "#cccccc" # ?
    colors["2"] = "#cc9999" # ?
    colors["3"] = "#ccffcc"
    colors["4"] = "#ffff99"
    colors["5"] = "#ffcc99"
    colors["6"] = "#ff99cc"
    colors["7"] = "#cc99ff"
    colors["8"] = "#99ccff"
    colors["9"] = "#6699ff"
    return colors[r].upper()

def name_for_rang(r):
    r = str(r)
    _rangs = {}
    _rangs["1"] = "новичок"
    _rangs["2"] = "любитель"
    _rangs["3"] = "таксист"
    _rangs["4"] = "профи"
    _rangs["5"] = "гонщик"
    _rangs["6"] = "маньяк"
    _rangs["7"] = "супермен"
    _rangs["8"] = "кибергонщик"
    _rangs["9"] = "экстракибер"
    return _rangs[r]

def forum_link_profile(_id):
    _value = table[_id]
    _name = list_get_last_not_none(_value["name"])
    _color = color_for_rang_fg(list_get_last_not_none(_value["rang"]))
    profile_template = "[url=https://klavogonki.ru/profile/{}][color={}][b][u]{}[/u][/b][/color][/url]"
    return profile_template.format(_id, _color, _name)

def rangs():
    return range(1,10)

# header

format_header = workbook.add_format()
format_header.set_align('center')
format_header.set_align('vcenter')
format_header.set_bg_color('#d9d9d9')
format_header.set_border(1)
format_header.set_bold()

n_columns = len(df.columns)
column_of_first_game = 3
column_of_last_game = column_of_first_game + num_games - 1

# https://stackoverflow.com/questions/26722813/how-do-i-add-formats-to-existing-format-objects-on-the-fly-using-xlsxwriter/55269950#55269950
def copy_format(book, fmt):
    properties = [f[4:] for f in dir(fmt) if f[0:4] == 'set_']
    dft_fmt = book.add_format()
    return book.add_format({k : v for k, v in fmt.__dict__.items() if k in properties and dft_fmt.__dict__[k] != v})

format_header_without_bottom = copy_format(workbook, format_header)
format_header_without_bottom.set_bottom(0)
for i_column in range(n_columns):
    i_row = 0
    cell_string = df.iat[i_row, i_column]
    worksheet.write(i_row+y_offset, i_column+x_offset, cell_string, format_header_without_bottom)

worksheet.merge_range(0+y_offset,0+x_offset,1+y_offset,0+x_offset, df[0][0], format_header)
worksheet.merge_range(0+y_offset,1+x_offset,1+y_offset,1+x_offset, df[1][0], format_header)
worksheet.merge_range(0+y_offset,2+x_offset,1+y_offset,2+x_offset, df[2][0], format_header)
_col = num_games + column_of_first_game
worksheet.merge_range(0+y_offset,_col+x_offset,1+y_offset,_col+x_offset, df[_col][0], format_header)
_col = num_games + column_of_first_game + 1
worksheet.merge_range(0+y_offset,_col+x_offset,1+y_offset,_col+x_offset, df[_col][0], format_header)

hidden_format = copy_format(workbook, format_header)
hidden_format.set_num_format(" ")
hidden_format.set_top(0)
for game in range(num_games):
    i_row = 1
    i_column = column_of_first_game + game
    cell_string = difficulty_dict[game]
    worksheet.write(i_row+y_offset, i_column+x_offset, cell_string, hidden_format)

worksheet.conditional_format(
    1+y_offset, column_of_first_game+x_offset, 1+y_offset, column_of_first_game+x_offset + num_games,
    {'type': 'icon_set',
     'icon_style': '5_ratings',
     'reverse_icons': True,
     'icons': [
               {'criteria': '>=', 'type': 'number', 'value': 1.05},
               {'criteria': '>=', 'type': 'number', 'value': 1.02},
               {'criteria': '>=', 'type': 'number', 'value': 0.98},
               {'criteria': '>=', 'type': 'number', 'value': 0.95},
    ]}
)

# показ решающих заездов при равенстве результатов
actual_win = {}
actual_lose = {}
for i, (_id, best_list) in enumerate(results_sorted):
    if i+1 < len(results_sorted):
        _, next_best_list = results_sorted[i+1]
        j = 0
        while j < len(best_list) and best_list[j]["speed"] == next_best_list[j]["speed"]:
            j += 1
        if j == len(best_list):
            j = j - 1
        if j > 0:
            _tmp = best_list[j]
            _tmp["level"] = j + 1
            actual_win[_id] = _tmp
    if i-1 > 0:
        _, prev_best_list = results_sorted[i-1]
        j = 0
        while j < len(best_list) and best_list[j]["speed"] == prev_best_list[j]["speed"]:
            j += 1
        if j == len(best_list):
            j = j - 1
        if j > 0:
            _tmp = best_list[j]
            _tmp["level"] = j + 1
            actual_lose[_id] = _tmp


# чтение результатов за все время
all_time_df = pd.read_excel('Slopestyle_prev.xlsx', sheet_name="За все время", index_col=None, header=None)

all_time_results = {}
all_time_ngames = {}
for _i,_id in enumerate(all_time_df[1][3:]):
    if not str(_id).isdigit():
        continue
    _row = 3 + _i
    _speed = [ all_time_df[_col][_row] for _col in range(5, 5+20) ]

    # подсчет результатов
    all_sublists = [ _speed[i:i+num_slopestyle_races] for i in range(len(_speed)-num_slopestyle_races+1)]
    for i,a in enumerate(all_sublists):
        for j,b in enumerate(a):
            all_sublists[i][j] = np.nan_to_num(float(b if b != ' ' else 0))

    for i, _list in enumerate(all_sublists):
        all_sublists[i] = sorted(_list)

    all_sublists = sorted(all_sublists, reverse = True)
    this_result = all_sublists[0][0]
    if this_result == 0: continue

    if _id not in all_time_results.keys():
        all_time_results[_id] = this_result
        all_time_ngames[_id] = 1
    else:
        if all_time_results[_id] < this_result:
            all_time_results[_id] = this_result
        all_time_ngames[_id] += 1

players_improved = []
for (_id, best_list) in results_sorted:
    __id = int(_id)
    if __id in all_time_results.keys() and all_time_results[__id] < best_list[0]["speed"] \
       and table[_id]["classified"] \
       and all_time_ngames[__id] >= min_num_results_for_award-1:
        players_improved.append(_id)

for position, (_id, best_list) in enumerate(results_sorted):
    value = table[_id]
    position = position + 1
    i_row = position + 1
    n_columns = len(df.columns)
    all_speed = list(filter(None, value["speed"]))
    best_speed = np.max(all_speed or 0)
    worst_speed = np.min(all_speed or 0)

    for i_column in range(n_columns):
        cell_string = df.iat[i_row, i_column]
        fmt = workbook.add_format()
        fmt.set_pattern(1)
        fmt.set_border(1)

        if i_column != 1:
            fmt.set_align('center')
        else:
            cell_string = " " + cell_string # сместить ник от края

        fmt.set_align('vcenter')

        if i_column in [0,1,2]:
            fmt.set_bold()

        if i_column == 2:
            if _id in players_improved:
                fmt.set_font_color('#ff0000')

        if i_column == column_of_last_game+1:
            fmt.set_num_format("0.00")
        if i_column == column_of_last_game+2:
            fmt.set_num_format("0.000")

        for j,i in enumerate(best_list):
            i_best = i["game"]
            col_best = i_best + column_of_first_game
            if i_column == col_best:
                fmt.set_bold()
                if j == 0:
                    fmt.set_underline()

        game = i_column - column_of_first_game
        if game in range(num_games):
            rang = list_get_last_not_none(value["rang"][:game+1]) or \
                   list_get_first_not_none(value["rang"][game:])
            fmt.set_bg_color(color_for_rang_bg(rang))
        else:
            fmt.set_bg_color(color_for_rang_bg(list_get_last_not_none(value["rang"])))

        if game in range(num_games) and cell_string == worst_speed:
            fmt.set_font_color('#808080')
        if game in range(num_games) and cell_string == best_speed:
            fmt.set_font_color('#0000ff')

        superscript = copy_format(workbook, fmt)
        superscript.set_font_script(1)
        #subscript = copy_format(workbook, fmt)
        #subscript.set_font_script(2)
        left_superscript = " "
        right_superscript = " "
        _i_row = i_row if value["classified"] else i_row+1

        if _id in actual_win.keys() and actual_win[_id]["game"]+column_of_first_game == i_column:
            right_superscript = str(actual_win[_id]["level"]) + u"\u200C"
        if _id in actual_lose.keys() and actual_lose[_id]["game"]+column_of_first_game == i_column:
            left_superscript = str(actual_lose[_id]["level"]) + u"\u200C"
        if right_superscript != " " or left_superscript != " ":
            cell_string = str(cell_string) or "  "
            worksheet.write_rich_string(_i_row+y_offset, i_column+x_offset, superscript, left_superscript, fmt, cell_string, superscript, right_superscript, fmt)
        else:
            worksheet.write(_i_row+y_offset, i_column+x_offset, cell_string, fmt)

n_classified = len([_id for _id,_best_list in results_sorted if table[_id]["classified"]])
if len(results_sorted) > n_classified:
    worksheet.merge_range(3+n_classified-1+y_offset, 0+x_offset, 3+n_classified-1+y_offset, len(df.columns)-1+x_offset, "Вне зачета", format_header)

forum_list = [forum_link_profile(_id) for _id in players_improved]
forum_text = "Результат улучшили: " + ", ".join(forum_list)
print(forum_text)
worksheet_forum = workbook.add_worksheet("Форум")
worksheet_forum.write(0, 0, forum_text)

speed_prev_df = pd.read_excel('Slopestyle_prev.xlsx', sheet_name="Зачет по скорости", usecols="DY", index_col=None, header=None)
slopestyle_number = speed_prev_df.iat[16,0]
slopestyle_forum_id = speed_prev_df.iat[17,0]
slopestyle_forum_post_number = 2

msg_row_index = 2
results_sorted_classified = list(filter(lambda x: table[x[0]]["classified"], results_sorted))
_msg_row_index = msg_row_index
for r in reversed(rangs()):
    _res_rang = list(filter(lambda x: int(list_get_last_not_none(table[x[0]]["rang"])) == r, results_sorted_classified))
    if len(_res_rang) > 0:
        _id = _res_rang[0][0]
        _str = "Лучший [b]{}[/b] - {}".format(name_for_rang(r), forum_link_profile(_id))
        worksheet_forum.write(_msg_row_index, 0, _str)
        _msg_row_index += 1
if _msg_row_index > msg_row_index:
    msg_row_index = _msg_row_index -1
else:
    msg_row_index -= 2

print("Номер слоупстайла:", slopestyle_number)
print("Тема на форуме:", slopestyle_forum_id)
row_slopestyle_number = 2 + msg_row_index
row_slopestyle_forum_id = 3 + msg_row_index
row_slopestyle_forum_post_number = 4 + msg_row_index
worksheet_forum.set_column(0,0, 20) # пошире
worksheet_forum.write(row_slopestyle_number, 0, "Номер слоупстайла:")
worksheet_forum.write(row_slopestyle_forum_id , 0, "Тема на форуме:")
worksheet_forum.write(row_slopestyle_forum_post_number, 0, "Номер поста:")
worksheet_forum.write(row_slopestyle_number, 1, slopestyle_number)
worksheet_forum.write(row_slopestyle_forum_id, 1, slopestyle_forum_id)
worksheet_forum.write(row_slopestyle_forum_post_number, 1, slopestyle_forum_post_number)

border_fmt =  workbook.add_format({"left": 2, "right": 2})
first_border_fmt = copy_format(workbook, border_fmt)
first_border_fmt.set_top(2)
start_row = msg_row_index + 6
msg_template = "__[Slopestyle №{}](https://klavogonki.ru/forum/events/{}/page1/#post{}) — {} (~~{}~~) зн/мин!__"
msg_pic = "![Улучшение результата](https://farm8.staticflickr.com/7852/47144419242_7db37374a8_o.jpg)"
cell_template = '",B${},"'
cell_slopestyle_number = cell_template.format(row_slopestyle_number+1)
cell_slopestyle_forum_id = cell_template.format(row_slopestyle_forum_id+1)
cell_slopestyle_forum_post_number = cell_template.format(row_slopestyle_forum_post_number+1)
formula_template = '=CONCATENATE("{}")'.format(msg_template.format(cell_slopestyle_number, cell_slopestyle_forum_id, cell_slopestyle_forum_post_number, "{}", "{}"))
for i, _id in enumerate(players_improved):
    speed_improved = results_dict[_id][0]["speed"]
    value = table[_id]
    name = list_get_last_not_none(value["name"])
    speed_old = int(all_time_results[int(_id)])
    msg = msg_template.format(slopestyle_number, slopestyle_forum_id, slopestyle_forum_post_number, speed_improved, speed_old)
    color = color_for_rang_fg(list_get_last_not_none(value["rang"]))
    fmt = workbook.add_format({"color": color})
    worksheet_forum.write(start_row + 5*i, 0, name, fmt)
    for i_row in range(5):
        i_row = i_row + start_row + 5*i
        worksheet_forum.write(i_row, 2, None, border_fmt)
    formula = formula_template.format(speed_improved, speed_old)
    worksheet_forum.write_formula(start_row + 5*i, 2, formula, first_border_fmt if i==0 else border_fmt, msg)
    worksheet_forum.write(start_row + 2 + 5*i, 2, msg_pic, border_fmt)
last_border_fmt = copy_format(workbook, border_fmt)
last_border_fmt.set_bottom(2)
worksheet_forum.write(start_row + 5*len(players_improved) - 1, 2, None, last_border_fmt)

# награждение
scores_for_position = {}
scores_for_position[1] = 4000
scores_for_position[2] = 3000
scores_for_position[3] = 2500
scores_for_position[4] = 2000
scores_for_position[5] = 1500
scores_for_position[6] = 1200
scores_for_position[7] = 900
scores_for_position[8] = 700
scores_for_position[9] = 500
scores_for_position[10] = 300

column_scores = 15
row_scores = msg_row_index + 5
worksheet_forum.set_column(column_scores, column_scores, 15) # пошире
bold_fmt = workbook.add_format({"bold": True})
worksheet_forum.write(row_scores, column_scores, "Очки", bold_fmt)
for position, (_id, best_list) in enumerate(results_sorted[:10]):
    position = position + 1
    row = row_scores + position
    value = table[_id]
    name = list_get_last_not_none(value["name"])
    if value["classified"] == False:
        break
    worksheet_forum.write(row, column_scores, name)
    worksheet_forum.write(row, column_scores + 1, scores_for_position[position])
    worksheet_forum.write(row, column_scores + 2, "{} место".format(position))

# вместо старого парсера
worksheet_oldtable_name = "Для за все время"
worksheet_oldtable = workbook.add_worksheet(worksheet_oldtable_name)
results_sorted_avg_speed = sorted(table.items(), key = lambda x: np.nan_to_num(x[1]["avg_speed"]), reverse = True)
results_sorted_avg_speed = list(filter(lambda x: np.nan_to_num(x[1]["avg_speed"])>0 , results_sorted_avg_speed))
results_sorted_avg_speed = list(filter(lambda x: x[1]["num_finishes"]>=min_player_finishes, results_sorted_avg_speed))

out_table = []
row = ["ID Профиля", "Место", "Ник", "Средняя\nскорость"]
for i in range(1,num_games+1):
    row.append(i)
row.extend(["", "comments_url", "marathon_link"])
out_table.append(row)
out_table.append(["" for _i in range(len(row))])

for position, (_id, best_list) in enumerate(results_sorted_avg_speed):
    value = table[_id]
    row = []
    row.append(int(_id))
    row.append(position+1)
    row.append(list_get_last_not_none(value["name"]))
    row.append(rounded_float(value["avg_speed"], 2))

    for i in value["speed"]:
        row.append(i or "")

    row.append("")
    row.append("")
    row.append("")
    out_table.append(row)

df = pd.DataFrame(out_table)
n_columns = len(df.columns)-3
n_rows = len(df.index)

for i, col in enumerate(df.columns):
    # максимальная ширина строк в столбце с учетом переносов
    column_len = df[col].astype(str).str.split(pat="\n").map(lambda x: max(x, key=len)).astype(str).str.len().max()
    column_len = max(column_len, len(str(col))) + 1
    if i == 2: # столбец с никами, жирный
        column_len *= 1.2
    worksheet_oldtable.set_column(i, i, column_len)

for i_column in range(len(df.columns)):
    i_row = 0
    cell_string = df.iat[i_row, i_column]
    fmt = copy_format(workbook, format_header)
    if i_column == 0:
        fmt.set_left(2)
    if i_column in [3, n_columns-1]:
        fmt.set_right(2)
    fmt.set_top(2)
    worksheet_oldtable.write(1, i_column, cell_string, fmt)

column_of_first_game = 4
for position, (_id, best_list) in enumerate(results_sorted_avg_speed):
    value = table[_id]
    position = position + 1
    i_row = position + 1

    for i_column in range(n_columns):
        cell_string = df.iat[i_row, i_column]
        fmt = workbook.add_format()
        fmt.set_pattern(1)
        fmt.set_border(1)

        if i_column not in [0,2]:
            fmt.set_align('center')

        fmt.set_align('vcenter')

        if i_column in [1,2]:
            fmt.set_bold()

        if i_column == 3:
            fmt.set_num_format("0.00")

        if i_column == 0:
            fmt.set_left(2)
        if i_column in [3, n_columns-1]:
            fmt.set_right(2)
        if i_row == n_rows-1:
            fmt.set_bottom(2)

        game = i_column - column_of_first_game
        if game in range(num_games):
            rang = list_get_last_not_none(value["rang"][:game+1]) or \
                   list_get_first_not_none(value["rang"][game:])
            fmt.set_bg_color(color_for_rang_bg(rang))
        elif game < 0:
            fmt.set_bg_color(color_for_rang_bg(list_get_last_not_none(value["rang"])))
        else:
            fmt.set_bg_color("white")

        worksheet_oldtable.write(i_row, i_column, cell_string, fmt)

    formula = 'HYPERLINK("https://klavogonki.ru/profile/{}/comments/#write","Написать комментарий")'.format(_id)
    worksheet_oldtable.write_formula(i_row, n_columns+1, formula)
    color = color_for_rang_fg(list_get_last_not_none(value["rang"]))
    name = list_get_last_not_none(value["name"])
    marathon_url = '[url=https://klavogonki.ru/profile/{}/stats/?gametype=marathon][color="{}"][b][u]{}[/u][/b][/color][/url]'.format(_id, color, name)
    worksheet_oldtable.write(i_row, n_columns+2, marathon_url)

writer.save()

## windows only
#
#import excel2img
#excel2img.export_img("out.xlsx", "out.png", sheet_name, None)

